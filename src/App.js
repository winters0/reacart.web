import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Layout from './components/Layout/Layout';
import ProductsList from './containers/Products/ProductsList';

class App extends Component {
  constructor(props){
    super(props);
    }
  
  render() {
    return (
      <div>
        <Layout>
          <ProductsList/>
          </Layout>
      </div>
    );

  }
}

export default App;
