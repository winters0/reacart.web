import React, { Component } from 'react';

class ProductsCatalogue extends Component {
	state = {
		products: {
			bread: 1,
            bacon: 1,
            cheese: 2,
            milk: 3
		}
	}
	render() {
		const products = this.state.products;

	return Object.entries(products);
	}
	
}

export default ProductsCatalogue;
